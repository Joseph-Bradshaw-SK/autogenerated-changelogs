import sys
import subprocess
import re

from_version = sys.argv[1]
to_version = sys.argv[2]

cmd = ('git', 'log', '--merges', '--pretty=format:------BEGIN------%n%as%n%an%n%s%n%b%n------END------', f'{from_version}...{to_version}')
stdout = subprocess.check_output(cmd).decode('utf-8')

commit_parser = re.compile(r"------BEGIN------\n(.*)\n(.*)\n(.*)\n(.*)(\n(\n|.)*?)------END------", re.MULTILINE)
mr_parser = re.compile(r"statkraft\/adam\/src!([0-9]*)")

matches = commit_parser.findall(stdout)

for match in matches:
    date = match[0]
    author = match[1]
    branch = match[2]
    title = match[3]
    body = match[4]

    mr_urls = []
    mrs = mr_parser.findall(body)
    for mr in mrs:
        mr_urls.append(f"http://gitlab.com/statkraft/adam/src/-/merge_requests/{mr}")

    print(f"{date} | {' '.join(mr_urls)} | {title} ({author})")




